/**
 * 
 */
package analysis;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * @author a9912577
 *
 */
public class Analysis {

	public static Double[] detrend(Double[] data, int window) {
		Double[] ret = new Double[data.length];


		SimpleRegression regression = new SimpleRegression();
		for (int i = 0; i < data.length; i++) {
			regression.addData(i,data[i]);
			ret[i] = 0d;
		}
		double slope = regression.getSlope();
		double intercept = regression.getIntercept();

		int z = data.length - window;
		SimpleRegression reg = new SimpleRegression();
		
		for (int i = 0; i < z; i++) {
			
			reg.addData(i,data.length);
			slope = regression.getSlope();
			intercept = regression.getIntercept();
			ret[i] = data[i] - (slope * i + intercept);
		}
		reg.clear();
		for (int i = z; i < (z + window); i++ ) {
			reg.addData(i,data[i]);
			slope = regression.getSlope();
			intercept = regression.getIntercept();
			ret[i] = data[i] - (slope * i + intercept);
		}
		return ret;

	}

	public static Double[] normalize(Double[] data, double min, double max) {
		Double[] ret = new Double[data.length];
		DescriptiveStatistics ds = new DescriptiveStatistics();
		for (int i = 0; i < data.length; i++) {
			ds.addValue(data[i]);
		}
		Double mmax = ds.getMax();
		Double mmin = ds.getMin();
		for (int i = 0; i < data.length; i++) {
			ret[i] = min + (((data[i] - mmin) * (max - min))/(mmax - mmin));
		}
		return ret;
	}
}
