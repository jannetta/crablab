package vsd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class VSD_Ops_Normalize extends JPanel  implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6743710938845301237L;
	JButton normalize;
	JTextField seriesfrom, seriesto, minimum, maximum;
	JLabel lbl_seriesto, lbl_seriesfrom, lbl_normalize, lbl_minimum, lbl_maximum;

	public VSD_Ops_Normalize() {
		normalize = new JButton("Normalize");
		seriesfrom = new JTextField("0",5);
		seriesto = new JTextField("0",5);
		minimum = new JTextField("-1",5);
		maximum = new JTextField("1",5);
		lbl_seriesfrom = new JLabel("Data Series from: ");
		lbl_seriesto = new JLabel("Data Series to: ");
		lbl_normalize = new JLabel("Normalize");
		lbl_minimum = new JLabel("Minimum value");
		lbl_maximum = new JLabel("Maximum value");
		normalize.addActionListener(this);
		JPanel panel = new JPanel();
		panel.add(lbl_seriesfrom);
		panel.add(seriesfrom);
		panel.add(lbl_seriesto);
		panel.add(seriesto);
		panel.add(lbl_minimum);
		panel.add(maximum);
		panel.add(lbl_maximum);
		panel.add(minimum);
		panel.add(normalize);
		add(panel, BorderLayout.CENTER);
		Border blackline = BorderFactory.createLineBorder(Color.black);;
		TitledBorder title = BorderFactory.createTitledBorder(blackline, "Normalize");
		title.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(title);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == normalize) {
			if (FileReader.getFile()==null) {
				JOptionPane.showMessageDialog(this, "Error: No data file loaded.");
			} else {
				double min = Double.valueOf(minimum.getText());
				double max = Double.valueOf(maximum.getText());
				int sf = Integer.valueOf(seriesfrom.getText());
				int st = Integer.valueOf(seriesto.getText());
				for (int i = sf; i <= st; i++) {
					VSD_data vsd_data = FileReader.getDataSeries(i);
					VSD_data normalized_vsd = new VSD_data();
					Double[] normalized = vsd_data.normalize(min,max);
					normalized_vsd.addArray(normalized);
					FileReader.addVsd_data(normalized_vsd,"X" + i + "normalized");
				}
			}
		}

	}
}
