package vsd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class VSD_Ops_Detrend extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5564816398739302672L;
	JButton detrend;
	JTextField window, seriesfrom, seriesto;
	JLabel lbl_window, lbl_seriesfrom, lbl_seriesto;

	public VSD_Ops_Detrend() {
		detrend = new JButton("Detrend");
		window = new JTextField("200",5);
		seriesfrom = new JTextField("0",5);
		seriesto = new JTextField("0",5);
		lbl_seriesfrom = new JLabel("Data Series from: ");
		lbl_seriesto = new JLabel("Data Series to: ");
		lbl_window = new JLabel("Windows: ");
		detrend.addActionListener(this);
		JPanel panel = new JPanel();
		panel.add(lbl_seriesfrom);
		panel.add(seriesfrom);
		panel.add(lbl_seriesto);
		panel.add(seriesto);
		panel.add(lbl_window);
		panel.add(window);
		panel.add(detrend);
		add(panel, BorderLayout.CENTER);
		Border blackline = BorderFactory.createLineBorder(Color.black);;
		TitledBorder title = BorderFactory.createTitledBorder(blackline, "Detrend");
		title.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(title);

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == detrend) {
			if (FileReader.getFile()==null) {
				JOptionPane.showMessageDialog(this, "Error: No data file loaded.");
			} else {
				int win = Integer.valueOf(window.getText());
				int sf = Integer.valueOf(seriesfrom.getText());
				int st = Integer.valueOf(seriesto.getText());
				for (int i = sf; i <= st; i++) {
					VSD_data vsd_data = FileReader.getDataSeries(i);
					VSD_data detrended_vsd = new VSD_data();
					Double[] detrended = vsd_data.detrend(Integer.valueOf(win));
					detrended_vsd.addArray(detrended);
					FileReader.addVsd_data(detrended_vsd,"X" + i + "detW" + win);
				}
			}
		}

	}
}
