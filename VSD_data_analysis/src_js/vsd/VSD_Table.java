package vsd;

import javax.swing.JTable;

public class VSD_Table extends JTable{

	/**
	 * Table for displaying VSD_data
	 */
	private static final long serialVersionUID = -2308670495144915487L;
	static VSD_TableModel vsd_model = new VSD_TableModel();

	public VSD_Table() {
		super(vsd_model);
		vsd_model.addTableModelListener(this);
		setAutoCreateColumnsFromModel(true);
		createDefaultColumnsFromModel();
	}
	
	public void fireTableDataChanged() {
		vsd_model.fireTableStructureChanged();
	}
}
