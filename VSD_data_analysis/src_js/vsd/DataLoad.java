package vsd;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.regression.SimpleRegression;

public class DataLoad {

	/**
	 * An array of columns holding the data. Usually column one will be time and column two will be the external recording
	 */
	private Column columns[];

	/**
	 * The number of columns for the dataset
	 */
	private int columnNumber;

	/**
	 * An array with the phase times for calculating averages
	 */
	//private ArrayList<Double> phaseTimes = new ArrayList<Double>();

	public DataLoad(int columnNumber) {
		this.columnNumber = columnNumber;
		columns = new Column[columnNumber];
		for (int i = 0; i < columnNumber; i++) {
			columns[i] = new Column();
		}
	}

	public Double[][] getDataLoad() {
		Double[][] ret = new Double[columnNumber][columns.length];
		for (int i = 0; i < columnNumber; i++) {
			ret[i] = columns[i].getArray();
		}
		return ret;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public void addColumnItem(Double item, int column) {
		columns[column].add(item);
	}

	public Column getColumn(int index) {
		return columns[index];
	}

	public Double getColumnItem(int column, int index) {
		return columns[column].get(index);
	}

	public int getColumnLength(int column) {
		return columns[column].size();
	}

	public Double[] getColumnArray(int index) {
		Double[] d = columns[index].getArray();
		return d;
	}


	/**
	 * @return If the columns are of equal length return true otherwise return false
	 */
	public boolean equalLength() {
		boolean equal = true;
		int colsize = getColumnLength(0);
		for (int i = 1; i < columnNumber; i++) {
			equal = (colsize == getColumnLength(i));
		}
		return equal;
	}

	public Double[][] detrend(Double[][] data, int window) {
		Double[][] ret = new Double[data.length][data[0].length];

		for (int cols = 2; cols < data.length; cols++) {
			Double[] x = getColumnArray(0);
			Double[] y = getColumnArray(cols);
			SimpleRegression regression = new SimpleRegression();
			for (int i = 0; i < x.length; i++) {
				regression.addData(x[i], y[i]);
			}
			double slope = regression.getSlope();
			double intercept = regression.getIntercept();

			for (int i = 0; i < x.length; i++) {
				y[i] -= intercept + (x[i] * slope);
			}
			/*int z = getColumnLength(0) - window;
			for (int i = (z+1); i < (z+window); i++ ) {
				y[i] = x[i] - (slope * i + intercept);
			}*/
			ret[cols-2] = y;
		}
		return ret;
	}

	public Double[][] smooth(Double[][] data, int window) {
		Double[][] ret = new Double[data.length][data[0].length];
		int h_window = window / 2;
		int start = 0;
		int end = 0;

		for (int cols = 0; cols < data.length; cols++) {
			Double[] y = data[cols];
			// for each row 
			for (int rows = 0; rows < y.length; rows++) {
				// determine range
				if (rows < h_window) {
					start = 0;
				} else {
					start = rows - h_window;
				}
				if (rows + h_window < y.length) {
					end = rows + h_window;
				} else {
					end = y.length;
				}
				DescriptiveStatistics ds = new DescriptiveStatistics();
				// add values
				for (int range = start; range < end; range++) {
					ds.addValue(y[range]);
				}
				// get minimum for range
				ret[cols][rows] = ds.getMean();
			}
		}


		return ret;
	}


	public Double[][] slopes(Double[][] data, int window) {
		Double[][] ret = new Double[data.length][data[0].length];
		int h_window = window/2;
		int start = 0;
		int end = 0;
		// generate a time series
		Double[] x = new Double[data[0].length];
		for (int t = 0; t < data[0].length; t++) {
			x[t] = (double)t;
		}
		// for each column and each row of data, calculate slopes
		for (int cols = 0; cols < data.length; cols++) {
			Double[] y = data[cols];
			// for each row 
			for (int rows = 0; rows < y.length; rows++) {
				// determine range
				if (rows < h_window) {
					start = 0;
				} else {
					start = rows - h_window;
				}
				if (rows + h_window < y.length) {
					end = rows + h_window;
				} else {
					end = y.length;
				}
				SimpleRegression regression = new SimpleRegression();
				for (int range = start; range < end; range++) {
					regression.addData(x[range], y[range]);
				}
				double slope = regression.getSlope();
				ret[cols][rows] = slope;
			}
		}
		return ret;
	}

	public Double[][] mindata(Double[][] data, int window) {
		
		int h_window = window / 2;
		int start = 0;
		int end = 0;
		Double[][] ret = new Double[data.length][data[0].length];
		// for each column
		for (int cols = 0; cols < data.length; cols++) {
			Double[] y = data[cols];
			// for each row 
			for (int rows = 0; rows < y.length; rows++) {
				// determine range
				if (rows < h_window) {
					start = 0;
				} else {
					start = rows - h_window;
				}
				if (rows + h_window < y.length) {
					end = rows + h_window;
				} else {
					end = y.length;
				}
				DescriptiveStatistics ds = new DescriptiveStatistics();
				// add values
				for (int range = start; range < end; range++) {
					ds.addValue(y[range]);
				}
				// get minimum for range
				if (ds.getMin()==y[rows]) {
					ret[cols][rows] = 1d;
				} else {
					ret[cols][rows] = 0d;
				}
			}
		}
		return ret;
	}

	public Double[][] detrend_pa(Double[][] data, int window) {
		int h_window = window / 2;
		int start = 0;
		int end = 0;
		Double[][] ret = new Double[columnNumber-2][getColumnLength(0)];
		// for each column
		for (int cols = 2; cols < columnNumber; cols++) {
			Double[] y = getColumnArray(cols);
			// for each row 
			for (int rows = 0; rows < y.length; rows++) {
				// determine range
				if (rows < h_window) {
					start = 0;
				} else {
					start = rows - h_window;
				}
				if (rows + h_window < y.length) {
					end = rows + h_window;
				} else {
					end = y.length;
				}
				DescriptiveStatistics ds = new DescriptiveStatistics();
				// add values
				for (int range = start; range < end; range++) {
					ds.addValue(y[range]);
				}
				double mean = ds.getMean();
				ret[cols-2][rows] = y[rows] - mean;
			}
		}
		return ret;
	}

}
