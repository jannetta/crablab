package vsd;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class VSD_Operations extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7798354784699178543L;

	VSD_Ops_Normalize norm = new VSD_Ops_Normalize();
	VSD_Ops_Average ave = new VSD_Ops_Average();
	VSD_Ops_Smooth smth = new VSD_Ops_Smooth();
	VSD_Ops_Detrend det = new VSD_Ops_Detrend();

	public VSD_Operations() {
		super(new BorderLayout());
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(ave);
		add(norm);
		add(smth);
		add(det);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
