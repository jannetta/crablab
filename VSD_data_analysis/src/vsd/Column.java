package vsd;

import java.util.ArrayList;

public class Column extends ArrayList<Double> {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Empty public constructor
	 */
	public Column() {
		
	}
	
	
	/**
	 * @return Return this Column as an array of Double
	 */
	public Double[] getArray() {
		Double[] a = new Double[this.size()];
		for (int i = 0; i < a.length; i++) {
			a[i] = this.get(i);
		}
		return a;
	}
}
