package vsd;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;


public class VSD_gui_FileReader extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1385505049864827492L;
	private FileReader fr = FileReader.getInstance();
	static private final String newline = "\n";
	JButton openButton, saveButton, nameButton, phaseList;
	JTextField phases = new JTextField("0",4);
	JTextArea log;
	JFileChooser fc;

	public VSD_gui_FileReader() {
		super(new BorderLayout());

		//Create the log first, because the action listeners
		//need to refer to it.
		log = new JTextArea(5,20);
		log.setMargin(new Insets(5,5,5,5));
		log.setEditable(false);
		JScrollPane logScrollPane = new JScrollPane(log);

		//Create a file chooser
		fc = new JFileChooser();

		//Create the open button.  We use the image from the JLF
		//Graphics Repository (but we extracted it from the jar).
		openButton = new JButton("Open File",
				createImageIcon("images/folder-horizontal-open.png"));
		openButton.addActionListener(this);

		//Create the save button.  We use the image from the JLF
		//Graphics Repository (but we extracted it from the jar).
		saveButton = new JButton("Save File",
				createImageIcon("images/disk.png"));
		saveButton.addActionListener(this);

		//Name the file to save to.
		nameButton = new JButton("Output filename",
				createImageIcon("images/disk.png"));
		nameButton.addActionListener(this);

		//Name the file to save to.
		phaseList = new JButton("Phases filename",
				createImageIcon("images/folder-horizontal-open.png"));
		phaseList.addActionListener(this);

		//For layout purposes, put the buttons in a separate panel
		JPanel buttonPanel = new JPanel(); //use FlowLayout
		buttonPanel.add(openButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(nameButton);
		buttonPanel.add(phaseList);
		buttonPanel.add(phases);

		//Add the buttons and the log to this panel.
		add(buttonPanel, BorderLayout.PAGE_START);
		add(logScrollPane, BorderLayout.CENTER);
	}

	public void actionPerformed(ActionEvent e) {

		//Handle open button action.
		if (e.getSource() == openButton) {
			openFile();
			//Handle save button action.
		} else if (e.getSource() == saveButton) {
			saveFile();
			// Handle save-file-as button
		} else if (e.getSource() == nameButton) {
			saveAsFile();
			// Handle read-phase-list button
		} else if (e.getSource() == phaseList) {
			phaseList();

		}
	}

	/**
	 * Read file with phase times for a specific series
	 */
	private void phaseList() {
		int returnVal = fc.showOpenDialog(VSD_gui_FileReader.this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			//This is where a real application would open the file.
			log.append("Opening: " + file.getName() + "." + newline);
			log.append(file.getPath() + newline);
			int p = Integer.valueOf(phases.getText());
			String result = FileReader.readPhaseList(file, p);
			if (result.startsWith("Error")) {
				JOptionPane.showMessageDialog(this, "Error: No data loaded. Phases are to be loaded for a specific data series.");
			}
		} else {
			log.append("Open command cancelled by user." + newline);
		}
		log.setCaretPosition(log.getDocument().getLength());
	}

	/**
	 * Filename to save to
	 */
	private void saveAsFile() {
		int returnVal = fc.showSaveDialog(VSD_gui_FileReader.this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			FileReader.setSaveAs(file);
			//This is where a real application would save the file.
			log.append("Saving: " + file.getName() + "." + newline);
		} else {
			log.append("Save command cancelled by user." + newline);
		}
		log.setCaretPosition(log.getDocument().getLength());
	}

	/**
	 * Save the data to file
	 */
	private void saveFile() {
		try {
			String result = fr.saveFile();
			if (result.startsWith("Error")) {
				JOptionPane.showMessageDialog(this,result);
			} else 
				log.append("File saved."+ newline);
		} catch (FileNamesNotSetException e1) {
			JOptionPane.showMessageDialog(this,"Error: Filename not set.");
		}
	}

	/**
	 * Open data file
	 */
	private void openFile() {
		int returnVal = fc.showOpenDialog(VSD_gui_FileReader.this);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			//This is where a real application would open the file.
			log.append("Opening: " + file.getName() + "." + newline);
			log.append(file.getPath());
			FileReader.setFile(file);
			String ret = FileReader.readFile();
			if (ret.startsWith("Error:")) JOptionPane.showMessageDialog(this,ret);;
		} else {
			log.append("Open command cancelled by user." + newline);
		}
		log.setCaretPosition(log.getDocument().getLength());
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = VSD_gui_FileReader.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}

}
