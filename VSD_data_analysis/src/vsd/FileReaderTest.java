package vsd;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FileReaderTest {
	String datafile = "20140127_Post.csv";
	String phasefile = "20140127_Post.txt";
	String outfile = "20140127_Post_ave.csv";
	FileReader fr = new FileReader();


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		FileReader.setFile(new File(datafile));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testreadPhaseList() {
		String b = FileReader.readPhaseList(new File(phasefile),3);
		assertTrue(b.equals(""));
		String c = FileReader.readFile();
		assertTrue(c.equals(""));
	}
	

	
}
