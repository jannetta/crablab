package vsd;
import javax.swing.table.AbstractTableModel;

public class VSD_TableModel extends AbstractTableModel  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 737198854514334060L;
	
	@Override
	public int getColumnCount() {
		return FileReader.getColCount();
	}

	@Override
	public int getRowCount() {
		return FileReader.getRowCount();
	}

	@Override
	public String getColumnName(int col) {
		return FileReader.getHeaders()[col];
	}

	@Override
	
	public Object getValueAt(int row, int col) {
//		return FileReader.getData()[row][col];//data[row][col]; // Inefficient
		return FileReader.getDataAt(row, col);//data[row][col];
	}

	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}


	@Override
	public boolean isCellEditable(int row, int column) {
		if (column == 0 || column == 1) {
			return false;
		} else {
			return true;
		}
	}
}

