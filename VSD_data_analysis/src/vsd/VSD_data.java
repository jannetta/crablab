package vsd;

import java.util.ArrayList;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import analysis.Analysis;

/**
 * @author a9912577
 *
 */
public class VSD_data {

	private String header = "";
	private ArrayList<Double> time;
	private ArrayList<Double> data;
	private ArrayList<Integer> phase_list;

	public VSD_data() {

	}

	public VSD_data(Double[] d) {
		for (int i = 0; i < d.length; i++) {
			time.add(Double.valueOf(i));
		}
	}

	public ArrayList<Integer> getPhase_list() {
		return phase_list;
	}

	public void setPhase_list(ArrayList<Integer> list) {
		phase_list = list;
	}

	public int getLength() {
		return data.size();
	}

	/**
	 * @return the header of this data series
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header set header of this data series
	 */
	public void setHeader(String header) {
		this.header = header;
	}


	/**
	 * Add a value and its time point to the data set
	 * @param t the time point
	 * @param d the data value
	 */
	public void addValue(Double t, Double d) {
		if (this.data == null) data = new ArrayList<Double>();
		if (this.time == null) time = new ArrayList<Double>();
		time.add(t);
		data.add(d);
	}

	/**
	 * @param d An array to fill the data series with. The time series will be generated as 0 to the length of the array.
	 */
	public void addArray(Double[] d) {
		for (int i = 0; i < d.length; i++) {
			addValue(Double.valueOf(i),d[i]);
		}
	}

	/**
	 * @return the data series as an array of Double.
	 */
	public Double[] getDataArray() {
		Double[] ret = new Double[data.size()];
		for (int i = 0; i < data.size(); i++) {
			ret[i] = data.get(i);
		}
		return ret;
	}

	/**
	 * @return the time of data series as an array of Double.
	 */
	public Double[] getTimeArray() {
		Double[] ret = new Double[time.size()];
		for (int i = 0; i < time.size(); i++) {
			ret[i] = time.get(i);
		}
		return ret;
	}

	/**
	 * Clears all data.
	 */
	public void clear() {
		time.clear();
		data.clear();
	}

	/**
	 * @param window
	 * @return
	 */
	public Double[] detrend(int window) {
		Double ret[] = Analysis.detrend(getDataArray(), window);
		return ret;
	}

	public Double[] normalize(double min, double max) {
		Double[] ret = new Double[data.size()];
		ret = Analysis.normalize(getDataArray(), min, max);
		return ret;
	}

	public int shortestPhase() {
		Integer shortest = 2147483647;
		System.out.println("Size of phase list: " + phase_list.size());
		for (int i = 0; i < phase_list.size()-1; i++) {
			if (phase_list.get(i+1) - phase_list.get(i) < shortest) 
				shortest = phase_list.get(i+1) - phase_list.get(i);
		}
		return shortest.intValue();
	}


	/**
	 * Smooth the data
	 * @param window The sliding window to be used for smoothing
	 * @return the smoothed data as an array of Double
	 */
	public Double[] smooth(int window) {
		Double[] ret = new Double[data.size()];
		int h_window = window/2;
		int start = 0;
		int end = 0;
		for (int rows = 0; rows < data.size(); rows++) {
			// determine range
			if (rows < h_window) {
				start = 0;
			} else {
				start = rows - h_window;
			}
			if (rows + h_window < data.size()) {
				end = rows + h_window;
			} else {
				end = data.size();
			}
			DescriptiveStatistics ds = new DescriptiveStatistics();
			// add values
			for (int range = start; range < end; range++) {
				ds.addValue(data.get(range));
			}
			// get minimum for range
			ret[rows] = ds.getMean();
	
		}
		return ret;
	}

	public Double[] calculateAverage(int phases, int prepoints, int window) {
		//The shortest phase of all phases
		int shortest = shortestPhase();
		// The number of phases in the phases file
		int cycles = phase_list.size();
		// The return array - thus the averaged data
		System.out.println("Phases: " + phases);
		System.out.println("Shortest: " + shortest);
		System.out.println("Prepoints: " + prepoints);
		Double[] ret = new Double[phases * shortest + prepoints];
		//Detrend data
		Double[] temp = Analysis.detrend(getDataArray(), window);
		//Normalise data
		temp = Analysis.normalize(temp, 0, 1);

		int datapoints = shortest * phases + prepoints;
		for (int s = 0; s < datapoints; s++) {
			System.out.println("datapoints: " + datapoints);
			DescriptiveStatistics ds = new DescriptiveStatistics();
			for (int p = 0; p < cycles - phases; p++) {
				ds.addValue(data.get(phase_list.get(p) - prepoints + s));
			}
			ret[s] = ds.getMean(); 
		}
		return ret;
	}

}
