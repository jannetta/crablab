package vsd;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class VSD_TabbedPane extends JPanel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6467179666219911107L;
	JTabbedPane tabbedPane = new JTabbedPane();
	VSD_Table datatable = new VSD_Table();

	public VSD_TabbedPane()
	{
		super(new GridLayout(1, 1));

		ImageIcon icon = createImageIcon("images/middle.gif");
		ImageIcon gear = createImageIcon("images/gear.png");
		ImageIcon folder = createImageIcon("images/blue-folder.png");
		ImageIcon table = createImageIcon("images/table.png");

		
		VSD_gui_FileReader vsdfr = new VSD_gui_FileReader();
		tabbedPane.addTab("File Ops", folder, vsdfr, "Open and Save Files");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);

		
		VSD_Operations actions = new VSD_Operations();
		tabbedPane.addTab("Operations", gear, actions,
				"Operations to be executed on data");
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);

		
		JScrollPane sp = new JScrollPane(datatable);
		tabbedPane.addTab("Data", table, sp, "Data table");
		tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		// if (FileReader.countSeries()==0) tabbedPane.setEnabledAt(2,false);

		
		JComponent panel4 = makeTextPanel("Panel #4 (has a preferred size of 410 x 50).");
		panel4.setPreferredSize(new Dimension(410, 50));
		tabbedPane.addTab("Tab 4", icon, panel4, "Does nothing at all");
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);

		// Add the tabbed pane to this panel.
		add(tabbedPane);

		// The following line enables to use scrolling tabs.
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}

	public void enableTab(int i)
	{
		tabbedPane.setEnabledAt(i, true);
	}

	public void tableChange()
	{
		datatable.fireTableDataChanged();
	}

	protected JComponent makeTextPanel(String text)
	{
		JPanel panel = new JPanel(false);
		JLabel filler = new JLabel(text);
		filler.setHorizontalAlignment(JLabel.CENTER);
		panel.setLayout(new GridLayout(1, 1));
		panel.add(filler);
		return panel;
	}

	/** Returns an ImageIcon, or null if the path was invalid. */
	protected static ImageIcon createImageIcon(String path)
	{
		java.net.URL imgURL = VSD_TabbedPane.class.getResource(path);
		if (imgURL != null)
		{
			return new ImageIcon(imgURL);
		} else
		{
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
}
