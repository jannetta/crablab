package vsd;
import java.awt.BorderLayout;
import javax.swing.JFrame;


public class VSD_gui extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	FileReader fr = FileReader.getInstance();;
	VSD_TabbedPane tabbedpane = new VSD_TabbedPane();

	public VSD_gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(tabbedpane, BorderLayout.CENTER);
        //Display the window.
        pack();
        setVisible(true);
        FileReader.guiEnabler(tabbedpane);
	}

}
