package vsd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class VSD_Ops_Smooth extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2455954412056242562L;
	JButton smooth;
	JTextField window, seriesfrom, seriesto;
	JLabel lbl_series, lbl_window, lbl_seriesfrom, lbl_seriesto;

	public VSD_Ops_Smooth() {
		smooth = new JButton("Smooth");
		window = new JTextField("200",5);
		seriesfrom = new JTextField("0",5);
		seriesto = new JTextField("0",5);
		lbl_series = new JLabel("Data Series: ");
		lbl_window = new JLabel("Windows: ");
		lbl_seriesfrom = new JLabel("Data Series from: ");
		lbl_seriesto = new JLabel("Data Series to: ");
		smooth.addActionListener(this);
		JPanel panel = new JPanel();
		panel.add(lbl_seriesfrom);
		panel.add(seriesfrom);
		panel.add(lbl_seriesto);
		panel.add(seriesto);
		panel.add(lbl_window);
		panel.add(window);
		panel.add(smooth);
		add(panel, BorderLayout.CENTER);
		Border blackline = BorderFactory.createLineBorder(Color.black);;
		TitledBorder title = BorderFactory.createTitledBorder(blackline, "Smooth");
		title.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(title);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == smooth) {
			if (FileReader.getFile()==null) {
				JOptionPane.showMessageDialog(this, "Error: No data file loaded.");
			} else {
				int win = Integer.valueOf(window.getText());
				int sf = Integer.valueOf(seriesfrom.getText());
				int st = Integer.valueOf(seriesto.getText());
				for (int i = sf; i <= st; i++) {
					VSD_data vsd_data = FileReader.getDataSeries(i);
					VSD_data smoothed_vsd = new VSD_data();
					Double[] smoothed = vsd_data.smooth(Integer.valueOf(win));
					smoothed_vsd.addArray(smoothed);
					FileReader.addVsd_data(smoothed_vsd,"X" + i + "smoothW" + win);
				}
			}
		}
	}

}
