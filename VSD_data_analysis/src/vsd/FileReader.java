package vsd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Jannetta S Steyn
 *
 */
public class FileReader {

	private static FileReader instance = null;
	private static File filename;
	private static File saveAs;
	private static ArrayList<VSD_data> vsd_data_list = new ArrayList<VSD_data>();
	private static File phasefile;
	private static VSD_TabbedPane tabbedPane;

	/**
	 * @return the FileReader singleton
	 */
	public synchronized static FileReader getInstance() {
		if(instance == null) instance = new FileReader();
		return instance;
	}

	/**
	 * @return the number of columns in the data set
	 */
	public static int getColCount() {
		System.out.println("VSD length: " + vsd_data_list.size());
		return vsd_data_list.size();
	}

	/**
	 * @return the number of rows in the data set
	 */
	public static int getRowCount() {
		if (vsd_data_list.isEmpty()) return 0;
		else return vsd_data_list.get(0).getLength();
	}

	/**
	 * Clear the array list of data series 
	 */
	public static void clear() {
		vsd_data_list.clear();
		saveAs = null;
	}

	/*public static int getDataSeriesCount() {
		return vsd_data_list.size();
	}*/

	/**
	 * @return the phase file
	 */
	public static File getPhasefile() {
		return phasefile;
	}

	/**
	 * @return the headers of the columns as an array of String
	 */
	public static String[] getHeaders() {
		String[] ret = new String[vsd_data_list.size()];
		for (int i = 0; i < vsd_data_list.size(); i++) {
			ret[i] = vsd_data_list.get(i).getHeader();
		}
		return ret ;
	}

	/*public static int getDataListLength() {
		if (vsd_data_list.isEmpty())
			return 0;
		else 
			return vsd_data_list.get(0).getLength();
	}*/

	/**
	 * @return the data as a two dimensional array of Object[row][column]
	 */
	public static Object[][] getData() {
		int cols = vsd_data_list.size();
		int rows = vsd_data_list.get(0).getLength();
		Double[][] data = new Double[rows][cols];
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < cols; c++) {
				data[r][c] = vsd_data_list.get(c).getDataArray()[r];
			}
		}
		return data;
	}
	
	/**
	 * @param row
	 * @param col
	 */
	public static Object getDataAt(int row, int col)
	{
		return vsd_data_list.get(col).getDataArray()[row];
	}

	/**
	 * @return filename data was read from
	 */
	public static File getFile() {
		return filename;
	}

	/**
	 * @param index the index of the data series to return
	 * @return the data series as type VSD_data
	 */
	public static VSD_data getDataSeries(int index) {
		return vsd_data_list.get(index);
	}

	/**
	 * @param fn the filename containing the phases
	 * @param ave_phases the number of phases over which to do the average
	 * @return a message to indicate whether or not the file was read successfully. If the string starts with Error:, an error occurred during reading.
	 */
	public static String readPhaseList(File fn, int ave_phases) {
		phasefile = fn;
		String res = "";
		if (filename==null) {
			res = "Error: No data file loaded.";
		} else {
			ArrayList<Integer> phases = new ArrayList<Integer>();
			try {
				Scanner s = new Scanner(fn);
				while (s.hasNextLine()) {
					String line = s.nextLine();
					//Integer d = (int) (Double.valueOf(line)/1.5*1000);
					Integer d = Integer.valueOf(line);
					phases.add(d);
				}
				int data_list = vsd_data_list.size();
				System.out.println("Phases size: " + phases.size());
				for (int i = 1; i < data_list; i++) {
					vsd_data_list.get(i).setPhase_list(phases);
					
				}
				s.close();
			} catch (FileNotFoundException e) {
				res = "Error: Phases file not read.";
			}
			res = "";
		}
		return res;
	}

	public static void guiEnabler(VSD_TabbedPane pane) {
		tabbedPane = pane;
	}

	public static void enablePane(int i ) {
		tabbedPane.enableTab(i);
		tabbedPane.tableChange();
	}

	/**
	 * @param fn the filename to read data from
	 * @return the FileReader singleton
	 */
	public static FileReader getInstance(File fn) {
		filename = fn;
		if (instance == null) {
			instance = new FileReader();
		}
		return instance;
	}

	/**
	 * @return the number of data series 
	 */
	public static int countSeries() {
		return vsd_data_list.size();
	}

	/**
	 * @param vsd_data Add a data series to the array 
	 */
	public static void addVsd_data(VSD_data vsd_data, String header) {
		vsd_data.setHeader(header);
		FileReader.vsd_data_list.add(vsd_data);
	}

	/**
	 * @param fn filename to save data to
	 */
	public static void setSaveAs(File fn) {
		saveAs = fn;
	}

	public static void setPhasefile(File phasefile) {
		FileReader.phasefile = phasefile;
	}

	/**
	 * @param fn the filename to read data from
	 */
	public static void setFile(File fn) {
		filename = fn;
	}

	
	/**
	 * Parse file header. It is assumed to be 10 lines long and to start with 'average' or line 3.
	 * @param sc the scanner used to strip the header
	 * @throws FileNotFoundException 
	 */
	private static void readFileHeader(Scanner sc) throws FileNotFoundException
	{
		Scanner tempScanner = new Scanner(filename); //Create a temporary Scanner so that position isn't lost
		String line;
		line = tempScanner.nextLine();
		line = tempScanner.nextLine();
		line = tempScanner.nextLine(); // Go to 3rd line
		if (line.startsWith("average"))
		{
			for (int i=0; i<10; i++) // Read and dispose of the first 10 lines of header
			{
				sc.nextLine();
			}
		}
		tempScanner.close();
	}
	
	/**
	 * Read specified file and populate a VSD_data construct for each column
	 */
	public static String readFile() {
		String ret = filename.getName();
		clear();
		try {
			Scanner s = new Scanner(filename);
			readFileHeader(s);
			boolean init = true; // to show it is the first line read
			int tokens = 0;
			int time = 0;
			// Read file line by line
			while (s.hasNextLine()) {
				String line = s.nextLine();
				// Declare tokenizer to tokenize each line
				StringTokenizer st = new StringTokenizer(line, ",");
				// For first line generate headers
				if (init) {
					tokens = st.countTokens();
					for (int i = 0; i < tokens; i++) {
						vsd_data_list.add(new VSD_data());
						vsd_data_list.get(i).setHeader("X" + i);
					}
					init = false;
				} else {
					// R
					for (int i = 0; i < tokens; i++) {
						VSD_data vsd_data = vsd_data_list.get(i);
						String t = st.nextToken();
						vsd_data.addValue(Double.valueOf(time), Double.valueOf(t));
					}
					time++;
				}
			}
			enablePane(2);
		} catch (FileNotFoundException e) {
			ret = "Error: File not found.";
		}
		return ret;
	}

	/**
	 * @param dataseries Data series to save to file
	 * @throws FileNamesNotSetException Throws exception if the filename to save to has not been set.
	 */
	public String saveFile(int dataseries) throws FileNamesNotSetException {
		String ret="";
		if (saveAs==null) throw new FileNamesNotSetException();  else {
			Double[] time = vsd_data_list.get(dataseries).getTimeArray();
			Double[] data = vsd_data_list.get(dataseries).getDataArray();
			Double[] smooth = vsd_data_list.get(dataseries).smooth(100);

			PrintWriter pw;
			try {
				pw = new PrintWriter(saveAs);
				pw.println(vsd_data_list.get(dataseries).getHeader());
				for (int i=0; i < time.length; i++) {
					pw.println(time[i] + "," + data[i] + "," + smooth[i]);
				}

				pw.close();
			} catch (FileNotFoundException e) {
				ret = "Error: File output error";
			}
		}
		return ret;
	}


	/**
	 * Save all data series in data load to file
	 * @throws FileNamesNotSetException Throws exception if the filename to save to has not been set.
	 */
	public String saveFile() throws FileNamesNotSetException {
		String ret;
		int number_of_dataseries = countSeries();
		if (number_of_dataseries > 0) {
			Double[][] data_series = new Double[number_of_dataseries][];
			for (int i = 0; i < number_of_dataseries; i++) {
				data_series[i] = vsd_data_list.get(i).getDataArray();
			}
			int rows = 0;
			if (saveAs==null) throw new FileNamesNotSetException();  else {
				rows = data_series[0].length;
				ret = saveAs.getAbsolutePath();
				try {
					PrintWriter pw = new PrintWriter(saveAs);
					for (int i = 0; i < number_of_dataseries; i++) {
						pw.print(vsd_data_list.get(i).getHeader());
						if (i < number_of_dataseries-1) {
							pw.print(",");
						}
					}
					pw.println();
					for (int r=0; r < rows; r++) {
						for (int i = 0; i < number_of_dataseries; i++) {
							if (r < data_series[i].length)  
								pw.print(data_series[i][r]);
							else pw.print(0);
							if (i < number_of_dataseries-1) {
								pw.print(",");
							}
						}
						pw.println();
					}
					pw.close();

				} catch (FileNotFoundException e) {
					System.out.println("File output error");
					ret = "Error: File output error";
				}
			} 
		} else ret = "Error: No data available";
		return ret;
	}

}