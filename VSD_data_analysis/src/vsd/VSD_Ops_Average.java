package vsd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class VSD_Ops_Average extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3353681736603337029L;
	JButton average;
	JTextField seriesfrom, seriesto, window, phases, pre_points;
	JLabel lbl_seriesfrom, lbl_seriesto, lbl_window, lbl_phases, lbl_pre_points;

	public VSD_Ops_Average() {
		average = new JButton("Average");
		seriesfrom = new JTextField("0",5);
		seriesto = new JTextField("0", 5);
		window = new JTextField("100", 5);
		phases = new JTextField("3", 5);
		pre_points = new JTextField("100", 5);
		lbl_seriesfrom = new JLabel("Data Series from: ");
		lbl_seriesto = new JLabel("Data Series to: ");
		lbl_window = new JLabel("Window: ");
		lbl_phases = new JLabel("Phases: ");
		lbl_pre_points = new JLabel("Pre points: ");
		average.addActionListener(this);
		JPanel panel = new JPanel();
		panel.add(lbl_seriesfrom);
		panel.add(seriesfrom);
		panel.add(lbl_seriesto);
		panel.add(seriesto);
		panel.add(lbl_window);
		panel.add(window);
		panel.add(lbl_phases);
		panel.add(phases);
		panel.add(lbl_pre_points);
		panel.add(pre_points);
		panel.add(average);
		add(panel, BorderLayout.CENTER);
		Border blackline = BorderFactory.createLineBorder(Color.black);;
		TitledBorder title = BorderFactory.createTitledBorder(blackline, "Average");
		title.setTitleJustification(TitledBorder.CENTER);
		panel.setBorder(title);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == average) {
			if (FileReader.getFile() == null) {
				JOptionPane.showMessageDialog(this, "Error: No data file loaded.");
			} else {
				if (FileReader.getPhasefile()==null) {
					JOptionPane.showMessageDialog(this, "Error: No data phase file loaded.");
				} else {
					int win = Integer.valueOf(window.getText());
					int pha = Integer.valueOf(phases.getText());
					int pre = Integer.valueOf(pre_points.getText());
					int sf = Integer.valueOf(seriesfrom.getText());
					int st = Integer.valueOf(seriesto.getText());
					for (int i = sf; i <= st; i++) {
						VSD_data vsd_data = FileReader.getDataSeries(i);
						VSD_data averaged_vsd = new VSD_data();
						Double[] averaged = vsd_data.calculateAverage(pha, pre, win);
						averaged_vsd.addArray(averaged);
						FileReader.addVsd_data(averaged_vsd,"X" + i + "averaged");
					}
				}
			}
		}
	}

}
