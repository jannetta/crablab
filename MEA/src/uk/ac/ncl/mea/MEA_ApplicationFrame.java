package uk.ac.ncl.mea;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;

public class MEA_ApplicationFrame extends JFrame implements WindowListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MEA_MainMenuBar mainmenu;
	private MEA_Panel mea_panel;

	/**
	 * @param title
	 */
	public MEA_ApplicationFrame(final String title) {

		super(title);
		mea_panel = new MEA_Panel(this);
		mainmenu = new MEA_MainMenuBar(mea_panel);
		setJMenuBar(mainmenu);
		setContentPane(mea_panel);
		addWindowListener(this);
	}


	@Override
	public void windowActivated(WindowEvent e) {
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
        if (e.getWindow() == this) {
            dispose();
            System.exit(0);
        }
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		
	}




}
