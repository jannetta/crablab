package uk.ac.ncl.mea;

import java.awt.event.*;
import java.io.File;
import java.util.Hashtable;
import javax.swing.*;

public class MEA_MainMenuBar extends JMenuBar implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JMenu fileMenu = new JMenu("File");
	private static JMenuItem openFile = new JMenuItem("Open File ...");
	private static JMenuItem exit = new JMenuItem("Exit");
	private static JMenu filterMenu = new JMenu("Filter Menu");
	private static JMenu filterHighPass = new JMenu("High Pass Filter");
	private static JMenu filterLowPass = new JMenu("Low Pass Filter");
	private static JMenu filterFFT = new JMenu("FFT Filter");
	private static JMenu filterSmooth = new JMenu("Smoothing");
	private static JMenu removeMenu = new JMenu("Remove Series");
	private static JMenu displayMenu = new JMenu("Display Series");
	private final MEA_Panel mea_panel;
	private static final Hashtable<String, Integer> seriesKey = new Hashtable<String, Integer>();
	private static MEA_DataSet mea_dataset;

	public MEA_MainMenuBar(MEA_Panel panel) {
		super();
		this.mea_panel = panel;
		add(fileMenu);
		add(filterMenu);
		add(removeMenu);
		add(displayMenu);
		fileMenu.add(openFile);
		fileMenu.add(exit);
		filterMenu.add(filterHighPass);
		filterMenu.add(filterLowPass);
		filterMenu.add(filterFFT);
		filterMenu.add(filterSmooth);
		openFile.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals("Open File ...")) {
			final JFileChooser fc = new JFileChooser(new File("."));
			int returnVal = fc.showOpenDialog(mea_panel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File filename = fc.getSelectedFile();
				mea_panel.openFile(filename);
				setMenus();
				//???? set visible
				openFile.setEnabled(false);
			} else {
				System.out.println("Open command cancelled by user.");
			}
		}
		if (action.equals("Exit")) {
			System.exit(0);
		}
		if (action.toLowerCase().startsWith("High Pass".toLowerCase())) {
			// Get the series key
			String s = action.substring("High Pass".length()).trim();
			int seriesindex = seriesKey.get(s);

			// Run series through high pass filter and add the filtered series
			// as a new series
			String newKey = mea_dataset.highPass(seriesindex,
					mea_panel.getHLSlider() / 100.0);
			int i = seriesKey.size();

			if (!seriesKey.containsKey(newKey)) {
				// Add key to the hashtable
				seriesKey.put(newKey, new Integer(i));
				// Add new series (the filtered one) to the remove menu
				final JMenuItem remove;
				removeMenu
						.add(remove = new JMenuItem("Remove series " + newKey));
				remove.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem display;

				displayMenu.add(display = new JMenuItem("Display series "
						+ newKey));
				display.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem highpass;
				filterHighPass.add(highpass = new JMenuItem("High pass "
						+ newKey));
				highpass.addActionListener(this);

			}

			// Switch shapes off
			mea_panel.setSeriesShapesVisible(i, false);
			mea_panel.setSeriesVisible(i, true);
		}
		if (action.toLowerCase().startsWith("Remove series".toLowerCase())) {
			String s = action.substring("Remove series".length()).trim();
			mea_panel.setSeriesVisible(seriesKey.get(s), false);
		}
		if (action.toLowerCase().startsWith("Display series".toLowerCase())) {
			String s = action.substring("Display series".length()).trim();
			mea_panel.setSeriesVisible(seriesKey.get(s), true);
		}
		// Low pass filter
		if (action.toLowerCase().startsWith("Low Pass".toLowerCase())) {
			// Get the series key
			String s = action.substring("Low Pass".length()).trim();
			int seriesindex = seriesKey.get(s);

			// Run series through low pass filter and add the filtered series as
			// a new series
			String newKey = mea_dataset.lowPass(seriesindex,
					mea_panel.getHLSlider() / 10.0);
			int i = seriesKey.size();

			if (!seriesKey.containsKey(newKey)) {
				// Add key to the hashtable
				seriesKey.put(newKey, new Integer(i));

				// Add new series (the filtered one) to the remove menu
				final JMenuItem remove;
				removeMenu
						.add(remove = new JMenuItem("Remove series " + newKey));
				remove.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem display;
				displayMenu.add(display = new JMenuItem("Display series "
						+ newKey));
				display.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem lowpass;
				filterLowPass
						.add(lowpass = new JMenuItem("Low pass " + newKey));
				lowpass.addActionListener(this);
			}

			// Switch shapes off
			mea_panel.setSeriesShapesVisible(i, false);
			mea_panel.setSeriesVisible(i, true);
		}
		// FFT filter
		if (action.toLowerCase().startsWith("FFT".toLowerCase())) {
			// Get the series key
			String s = action.substring("FFT".length()).trim();
			int seriesindex = seriesKey.get(s);

			// Run series through low pass filter and add the filtered series as
			// a new series
			String newKey = mea_dataset.fft(seriesindex);
			int i = seriesKey.size();

			if (!seriesKey.containsKey(newKey)) {
				// Add key to the hashtable
				seriesKey.put(newKey, new Integer(i));

				// Add new series (the filtered one) to the remove menu
				final JMenuItem remove;
				removeMenu
						.add(remove = new JMenuItem("Remove series " + newKey));
				remove.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem display;
				displayMenu.add(display = new JMenuItem("Display series "
						+ newKey));
				display.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem fft;
				filterFFT.add(fft = new JMenuItem("FFT " + newKey));
				fft.addActionListener(this);
			}

			// Switch shapes off
			mea_panel.setSeriesShapesVisible(i, false);
			mea_panel.setSeriesVisible(i, true);
		}

	
	
	
		// Smoothing filter
		if (action.toLowerCase().startsWith("Smoothing".toLowerCase())) {
			// Get the series key
			String s = action.substring("Smoothing".length()).trim();
			int seriesindex = seriesKey.get(s);

			// Run series through low pass filter and add the filtered series as
			// a new series
			String newKey = mea_dataset.smoothing(seriesindex, mea_panel.getSmoothSlider());
			System.out.println(newKey);
			int i = seriesKey.size();

			if (!seriesKey.containsKey(newKey)) {
				// Add key to the hashtable
				seriesKey.put(newKey, new Integer(i));

				// Add new series (the filtered one) to the remove menu
				final JMenuItem remove;
				removeMenu
						.add(remove = new JMenuItem("Remove series " + newKey));
				remove.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem display;
				displayMenu.add(display = new JMenuItem("Display series "
						+ newKey));
				display.addActionListener(this);

				// Add new series (the filtered one) to the display menu
				final JMenuItem smooth;
				filterSmooth.add(smooth = new JMenuItem("Smoothing " + newKey));
				smooth.addActionListener(this);
			}

			// Switch shapes off
			mea_panel.setSeriesShapesVisible(i, false);
			mea_panel.setSeriesVisible(i, true);
		}

	
	}

	private void setMenus() {
		try {
			mea_dataset = MEA_DataSet.getInstanceOf();
			for (int i = 0; i < mea_dataset.getNumberOfSeries(); i++) {
				// Remove series
				final JMenuItem remove;
				removeMenu.add(remove = new JMenuItem("Remove series "
						+ mea_dataset.getKey(i)));
				remove.addActionListener(this);
				// Display series
				final JMenuItem display;
				displayMenu.add(display = new JMenuItem("Display series "
						+ mea_dataset.getKey(i)));
				display.addActionListener(this);
				// High Pass Filter series
				final JMenuItem highpass;
				filterHighPass.add(highpass = new JMenuItem("High Pass "
						+ mea_dataset.getKey(i)));
				highpass.addActionListener(this);
				// Low Pass Filter series
				final JMenuItem lowpass;
				filterLowPass.add(lowpass = new JMenuItem("Low Pass "
						+ mea_dataset.getKey(i)));
				lowpass.addActionListener(this);
				// FFT filter series
				final JMenuItem fft;
				filterFFT.add(fft = new JMenuItem("FFT "
						+ mea_dataset.getKey(i)));
				fft.addActionListener(this);
				// Smoothing filter series
				final JMenuItem smooth;
				filterSmooth.add(smooth = new JMenuItem("Smoothing  "
						+ mea_dataset.getKey(i)));
				smooth.addActionListener(this);

				// Add key to hastable
				seriesKey.put(mea_dataset.getKey(i), new Integer(i));
				
			}
		} catch (FileNotSetException e) {
			System.out.println("Open a file first");
		}

	}
}
