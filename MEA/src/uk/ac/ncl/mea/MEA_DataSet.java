package uk.ac.ncl.mea;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import uk.ac.ncl.filters.SeriesFilters;

/**
 * This is a singleton so that we can an instance from anywhere to
 * manipulate the data set.
 */
public class MEA_DataSet {


	private final static XYSeriesCollection dataset = new XYSeriesCollection();
	private static int number_of_series = 0;
	private static int instance = 0;
	private static MEA_DataSet mea_dataset;
	private static XYSeries series[];
	private static File filename;

	private MEA_DataSet(File name) {

	}

	public static MEA_DataSet getInstanceOf(File name) {
		if (instance == 0) {
			if (mea_dataset == null) {
				filename = name;
				mea_dataset = new MEA_DataSet(name);
				createDataSet(name);
			}
		}
		instance++;
		return mea_dataset;
	}

	public static MEA_DataSet getInstanceOf() throws FileNotSetException {
		//return getInstanceOf(new File("0005_h6_to_h11.txt"));
		if (filename==null) throw new FileNotSetException();
		return mea_dataset;
	}

	private static boolean createDataSet(File name) {
		try {
			// Open the file that is the first
			// command line parameter
			FileInputStream fstream = new FileInputStream(name);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			br.readLine(); // MC_DataToolASCIIconversion
			br.readLine(); // Empty line
			strLine = br.readLine(); // Labels
			br.readLine(); // Units
			StringTokenizer st = new StringTokenizer(strLine, "\t");
			number_of_series = st.countTokens() - 1;// -1 to account for 1st
			// column being time
			st.nextToken();
			series = new XYSeries[number_of_series];
			for (int i = 0; i < number_of_series; i++) {
				series[i] = new XYSeries(st.nextToken().trim());
			}
			int li=0;
			while ((strLine = br.readLine()) != null) {
				li++;
				
				// Print the content on the console
				st = new StringTokenizer(strLine, "\t");
				double time = (Double.valueOf(st.nextToken()).floatValue());
				double[] s = new double[number_of_series];
				for (int i = 0; i < number_of_series; i++) {
					s[i] = Double.valueOf(st.nextToken()).doubleValue();
				}
				for (int i = 0; i < number_of_series; i++) {
					series[i].add(time, s[i]);
				}

			}
			// Close the input stream
			for (int i = 0; i < number_of_series; i++) {
				dataset.addSeries(series[i]);
			}
			in.close();
			return true;
		} catch (NumberFormatException e) {
			System.out.println("An invalid file format, the information can't be parsed");
			return false;
		} catch (Exception e) {// Catch exception if any
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}

	}

	public XYSeriesCollection getDataSet() {
		return dataset;
	}

	public XYSeries getSeries(int i) {
		return series[i];
	}

	public int getNumberOfSeries() {
		return number_of_series;
	}

	public void removeSeries(int series) {
		dataset.removeSeries(series);
	}
	
	public String getKey(int index) {
		return series[index].getKey().toString();
	}
	
	public String highPass(int seriesindex, double alpha) {
		System.out.println("in high pass filter ...");
		XYSeries x = dataset.getSeries(seriesindex); // original series
		XYSeries y = SeriesFilters.highPass(x, alpha); // newly filtered series
		final String serieskey = x.getKey().toString() + "hp";
		int keyindex = dataset.getSeriesIndex(serieskey);
		boolean exists = (keyindex>=0);
		if (exists)  {
			dataset.removeSeries(keyindex); // existing filtered series
		}  
		dataset.addSeries(y);
		return y.getKey().toString();
		
	}

	public String lowPass(int seriesindex, double alpha) {
		System.out.println("in low pass filter ...");
		XYSeries x = dataset.getSeries(seriesindex);
		XYSeries y = SeriesFilters.lowPass(x, alpha); // dt = frequency in seconds
		int keyindex = dataset.getSeriesIndex(x.getKey().toString() + "lp");
		boolean exists = (keyindex>=0);
		if (exists) {
			dataset.removeSeries(keyindex); // existing filtered series
		}
		dataset.addSeries(y);
		System.out.println("low pass finished");
		return y.getKey().toString();
		
	}

	public String fft(int seriesindex) {
		System.out.println("in fft filter ...");
		XYSeries x = dataset.getSeries(seriesindex);
		XYSeries y = SeriesFilters.fft2(x); // dt = frequency in seconds
		int keyindex = dataset.getSeriesIndex(x.getKey().toString() + "fft");
		boolean exists = (keyindex>=0);
		if (exists) {
			dataset.removeSeries(keyindex); // existing filtered series
		}
		dataset.addSeries(y);
		System.out.println("fft finished");
		return y.getKey().toString();
		
	}
	
	public String smoothing(int seriesindex, int frame) {
		XYSeries x = dataset.getSeries(seriesindex);
		XYSeries y = SeriesFilters.smooth(x, frame);
		int keyindex = dataset.getSeriesIndex(x.getKey().toString() + "smth");
		boolean exists = (keyindex>=0);
		if (exists) {
			dataset.removeSeries(keyindex); // existing filtered series
		}
		dataset.addSeries(y);
		System.out.println("fft finished");
		return y.getKey().toString();
	}

}
