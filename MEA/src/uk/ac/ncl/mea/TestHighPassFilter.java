package uk.ac.ncl.mea;

import uk.ac.ncl.filters.AbstractFilter;
import uk.ac.ncl.filters.InterfaceFilter;

public class TestHighPassFilter {
	

	/**
	 * @param args
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		InterfaceFilter fhp = AbstractFilter.getInstance("uk.ac.ncl.filters.Filter_HighPass");
		System.out.println(fhp.getName());
		
	}

}
