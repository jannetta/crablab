package uk.ac.ncl.mea;

/**
 * Main MEA Application
 * 
 */
public class MEA  {


	/**
	 * Starting point for the demonstration application.
	 * 
	 * @param args
	 *            ignored.
	 */
	public static void main(final String[] args) {

		final MEA_ApplicationFrame mea = new MEA_ApplicationFrame("MEA Analyser");
		mea.pack();
		mea.setVisible(true);
		

	}

}
