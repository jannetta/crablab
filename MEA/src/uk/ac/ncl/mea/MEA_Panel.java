package uk.ac.ncl.mea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

public class MEA_Panel extends JPanel implements ActionListener, ChangeListener,
ChartChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JSlider hlpassfilterSlider = new JSlider(0,100,50);
	private JSlider smoothSlider = new JSlider(0,2000,1000);
	private MEA_DataSet mea_dataset;
	private XYLineAndShapeRenderer renderer;
	private XYDataset xy_dataset;
	private final JFrame parentframe;
	
	
	public MEA_Panel(JFrame parentframe) {
		super(new BorderLayout());
		this.parentframe = parentframe;
		hlpassfilterSlider.setMajorTickSpacing(10);
		hlpassfilterSlider.setMinorTickSpacing(1);
		hlpassfilterSlider.setPaintTicks(true);
		hlpassfilterSlider.setPaintLabels(true);
		hlpassfilterSlider.addChangeListener(this);
		smoothSlider.setMajorTickSpacing(100);
		smoothSlider.setMinorTickSpacing(10);
		smoothSlider.setPaintTicks(true);
		smoothSlider.setPaintLabels(true);
		smoothSlider.addChangeListener(this);
		this.setPreferredSize(new java.awt.Dimension(800,350));
		//openFile(new File("test"));
	}

	/**
	 * Creates a chart.
	 * 
	 * @param dataset
	 *            the data for the chart.
	 * 
	 * @return a chart.
	 */
	private JFreeChart createChart(final XYDataset dataset) {

		// create the chart...
		final JFreeChart chart = ChartFactory.createXYLineChart("MEA Analyser", // chart
																				// title
				"mS", // x axis label
				"\u00B5" + "V", // y axis label
				dataset, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		chart.setBackgroundPaint(Color.white);

		// final StandardLegend legend = (StandardLegend) chart.getLegend();
		// legend.setDisplaySeriesShapes(true);

		// get a reference to the plot for further customisation...
		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		// plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		int visible_up_to = 1;
		for (int i = 0; i < dataset.getSeriesCount(); i++) {
			renderer.setSeriesShapesVisible(i, false);
			if (i < visible_up_to) renderer.setSeriesVisible(i, true); else
				renderer.setSeriesVisible(i,false);
		}
		plot.setRenderer(renderer);

		// change the auto tick unit selection to integer units only...
		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		return chart;

	}

	public XYDataset getDataset() {
		return xy_dataset;
	}

	public void setSeriesVisible(int index, boolean visible) {
		renderer.setSeriesVisible(index, visible);
	}
	public void deleteSeries(int index) {
		mea_dataset.removeSeries(index);
	}
	
	public void setSeriesShapesVisible(int i, boolean visible) {
		renderer.setSeriesShapesVisible(i, visible);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("action performed.");
	}

	@Override
	public void chartChanged(ChartChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Chart changed.");
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Change Event.");
		System.out.println(hlpassfilterSlider.getValue());
	}
	
	public int getHLSlider() {
		return hlpassfilterSlider.getValue();
	}
	
	public int getSmoothSlider() {
		return smoothSlider.getValue();
	}
	
	public void openFile(File file) {
		mea_dataset = MEA_DataSet.getInstanceOf(file);
		xy_dataset = mea_dataset.getDataSet();
		renderer = new XYLineAndShapeRenderer();
		final JFreeChart jFreeChart = createChart(xy_dataset);
		final ChartPanel chartPanel = new ChartPanel(jFreeChart);
		add(hlpassfilterSlider, BorderLayout.CENTER);
		add(smoothSlider, BorderLayout.SOUTH);
		add(chartPanel, BorderLayout.NORTH);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		//chartPanel.repaint();
		//this.repaint();
		//parentframe.repaint();
		parentframe.pack();
	}
}
