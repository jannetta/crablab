package uk.ac.ncl.filters;

import org.jfree.data.xy.XYSeries;


public class Filter_HighPass extends AbstractFilter {

	private static int alpha=-1;
	private static int filterinstance = 0;
	private static Filter_HighPass filter_HighPass;
	
	public Filter_HighPass() {
		name="High Pass";
		suffix = "hp";
	}
	
	public void setAlpha(int a) {
		alpha = a;
	}

	@Override
	public XYSeries filter(XYSeries xyseries) throws FilterException {
		if (alpha<0) throw new AlphaNotSetException();
		// public static XYSeries highPass(XYSeries x, double dt, double RC) {
		//boolean update = false;
		int n = xyseries.getItemCount();
		XYSeries y = new XYSeries(xyseries.getKey().toString() + suffix);
		// double a = RC / (RC + dt);
		double a = alpha;
		y.add(xyseries.getDataItem(0));
		for (int i = 1; i < n; i++) {
			// y[i] := a * y[i-1] + a * (x[i] - x[i-1])
			y.add(xyseries.getDataItem(i).getXValue(), a
					* y.getDataItem(i - 1).getYValue()
					+ a
					* (xyseries.getDataItem(i).getYValue() - xyseries.getDataItem(i - 1)
							.getYValue()));
		}
		return y;
	}

}
