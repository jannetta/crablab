package uk.ac.ncl.filters;

import org.jfree.data.xy.XYSeries;

public interface InterfaceFilter {
	
	public String getName();
	public String getSuffix();
	public XYSeries filter(XYSeries xyseries) throws FilterException;
}
