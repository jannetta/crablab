package uk.ac.ncl.filters;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractFilter implements InterfaceFilter {
	protected String name;
	protected String suffix;
	private static final Map<String, InterfaceFilter> filters = new HashMap<String, InterfaceFilter>();

	public String getName() {
		return name;
	}

	public String getSuffix() {
		return suffix;
	}


	public static InterfaceFilter getInstance(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		if (filters.containsKey(className)) {
			return filters.get(className);
		}
		final Class<?> classN = Class.forName(className);
		final Object obj = classN.newInstance();
		if (!(obj instanceof InterfaceFilter)) {
			throw new IllegalArgumentException(
					"Class selected is not an InterfaceFilter");
		}
		final InterfaceFilter newFilter = (InterfaceFilter) obj;
		filters.put(className, newFilter);
		return newFilter;
	}
}
