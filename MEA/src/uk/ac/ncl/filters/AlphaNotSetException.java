package uk.ac.ncl.filters;

public class AlphaNotSetException extends FilterException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	AlphaNotSetException() {
		super();
	}
}
