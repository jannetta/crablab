package uk.ac.ncl.filters;

import org.jfree.data.xy.XYSeries;

//import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

public class SeriesFilters {

	// public static XYSeries highPass(XYSeries x, double dt, double RC) {
	public static XYSeries highPass(XYSeries x, double alpha) {
//		boolean update = false;
		int n = x.getItemCount();
		XYSeries y = new XYSeries(x.getKey().toString() + "hp");
		// double a = RC / (RC + dt);
		double a = alpha;
		y.add(x.getDataItem(0));
		for (int i = 1; i < n; i++) {
			// y[i] := a * y[i-1] + a * (x[i] - x[i-1])
			y.add(x.getDataItem(i).getXValue(), a
					* y.getDataItem(i - 1).getYValue()
					+ a
					* (x.getDataItem(i).getYValue() - x.getDataItem(i - 1)
							.getYValue()));
		}
		return y;
	}

	// Return RC low-pass filter output samples, given input samples,
	// time interval dt, and time constant RC
	// public static XYSeries lowPass(XYSeries x, double dt, double RC) {
	public static XYSeries lowPass(XYSeries x, double alpha) {
		int n = x.getItemCount();
		XYSeries y = new XYSeries(x.getKey().toString() + "lp");
		// double a = dt / (RC + dt);
		double a = alpha;
		y.add(x.getDataItem(0));
		for (int i = 1; i < n; i++) {
			// y[i] := a * x[i] + (1-a) * y[i-1]
			y.add(x.getDataItem(i).getXValue(),
					a * x.getDataItem(i).getYValue() + (1 - a)
							* x.getDataItem(i - 1).getYValue());
		}
		return y;
	}

	public static XYSeries dft(XYSeries v) {
		double[] r_data, i_data;
		int N = v.getItemCount();
		//double t_img, t_real;
		double twoPikOnN;
		double twoPijkOnN;
		// how many bits do we need?
		N = (int) log2(N);
		// Truncate input data to a power of two
		// length = 2**(number of bits).
		N = 1 << N;
		double twoPiOnN = 2 * Math.PI / N;
		// We truncate to a power of two so that
		// we can compare execution times with the FFT.
		// DFT generally does not need to truncate its input.
		r_data = new double[N];
		i_data = new double[N];
		XYSeries psd = new XYSeries(v.getKey().toString() + "ftt");
		System.out.println("Executing DFT on " + N + " points...");
		for (int k = 0; k < N; k++) {
			twoPikOnN = twoPiOnN * k;
			for (int j = 0; j < N; j++) {
				twoPijkOnN = twoPikOnN * j;
				r_data[k] += v.getY(j).doubleValue() * Math.cos(twoPijkOnN);
				i_data[k] -= v.getY(j).doubleValue() * Math.sin(twoPijkOnN);
			}
			r_data[k] /= N;
			i_data[k] /= N;
			psd.add(v.getDataItem(k).getXValue(), r_data[k] * r_data[k] + i_data[k] * i_data[k]);
		}
		return (psd);
	}
	
	public static XYSeries fft2(XYSeries x) {
		XYSeries y = new XYSeries(x.getKey().toString() + "ftt");
		int N = x.getItemCount();
		//DoubleFFT_1D fft = new DoubleFFT_1D(N);
		double[] d = new double[N*2];
		for (int i = 0; i < N; i++) {
			d[i] =  x.getDataItem(i).getYValue();
			
		}
		//fft.realForwardFull(d);
		for (int i = 0; i < N; i++) {
			y.add(x.getDataItem(i).getXValue(),d[i]);
		}
		return y;
	}
	
	public static XYSeries smooth(XYSeries x, int frame) {
		XYSeries y = new XYSeries(x.getKey().toString() + "smth");
		int N = x.getItemCount();
		for (int i = 0; i < N; i++) {
			int start = i - (frame / 2);
			int stop = i + (frame / 2);
			int avg = 0;
			if (start < 0) start = 0;
			if (stop > N) stop = N;
			for (int k = start; k < stop; k++) {
				avg += x.getDataItem(k).getYValue();
			}
			y.add(x.getDataItem(i).getX(),(avg/(stop-start)));
		}
		return y;
	}

	public static double log2(double num) {
		return ((double) Math.log(num) / (double) Math.log(2));
	}
}
