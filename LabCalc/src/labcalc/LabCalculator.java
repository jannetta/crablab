package labcalc;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * @author Jannetta S. Steyn
 * @date 2013
 *
 * Calculate quantities to make up solutions for voltage sensitive dyes.
 * Output such that it can be pasted directly into Wikimedia Wiki
 */
public class LabCalculator extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static InputScreen inputscreen = new InputScreen();
	
	public LabCalculator() {
		super();
		this.setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(640,480);
		add(inputscreen);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new LabCalculator();
	}

}
