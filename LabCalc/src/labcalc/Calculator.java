package labcalc;

/**
 * @author Jannetta S. Steyn
 * @date 2013
 * 
 * Doing the calculations
 */
public class Calculator {
	
	public static double fromMilli(double mg) {
		return mg * 0.001;
	}
	
	public static double toMilli(double g) {
		return g * Math.pow(10,3);
	}
	
	public static double fromMicro(double m) {
		return m * Math.pow(10, -6);
	}

	public static double toMicro(double m) {
		return m * Math.pow(10, 6);
	}

	public static double fromNano(double n) {
		return n * Math.pow(10, -9);
	}

	public static double toNano(double n) {
		return n * Math.pow(10, 9);
	}

	public static double fromPico(double p) {
		return p * Math.pow(10, -9);
	}

	public static double toPico(double p) {
		return p * Math.pow(10, 9);
	}
}
