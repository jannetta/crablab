package labcalc;

import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * @author Jannetta S. Steyn
 * @date 2013
 * 
 * The textbox containing the results
 *
 */
public class ResultBox extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextArea txa_results;
	
	public ResultBox(int height, int width)  {
		txa_results = new JTextArea(height, width);
		txa_results.setLineWrap(true);
		add(txa_results);
		setVisible(true);
	}
	
	public void setText(String t) {
		txa_results.setText(t);
	}
}
