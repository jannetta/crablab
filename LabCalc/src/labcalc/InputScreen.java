package labcalc;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * @author Jannetta S. Steyn
 * @date 2013
 *
 *	The Input screen
 */
public class InputScreen extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextField txt_molarity = new JTextField();
	JTextField txt_weight = new JTextField();
	JTextField txt_qty = new JTextField();
	JLabel lbl_molarity = new JLabel("Molarity (g/M)");
	JLabel lbl_weight = new JLabel("Weight of substance (g)");
	JLabel lbl_qty = new JLabel("Quantity of stock (microLitre)");
	JButton btn_calculate = new JButton("Calculate");
	JTextArea txa_results = new JTextArea(20,50);
	double dbl_molarity = 0;
	double dbl_weight = 0;
	String str_text;

	public InputScreen() {
		txt_molarity.setColumns(10);
		txt_weight.setColumns(10);
		txt_qty.setColumns(5);
		txt_qty.setText("20");
		txa_results.setLineWrap(true);
		btn_calculate.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				double molarity = Double.parseDouble(txt_molarity.getText());
				double weight = Double.parseDouble(txt_weight.getText());
				double qty = Double.parseDouble(txt_qty.getText());
				str_text = "";
				str_text+="Molar weight: " + txt_molarity.getText() + "\n\n";
				double solution = 1 / molarity * weight;
				str_text+=molarity + "g = 1 mol ==> " + weight + " = " 
				+ String.format("%10.4e",solution) + " mol\n\n";
				double millisolution = Calculator.toMilli(solution);//solution * 1000;
				str_text+=txt_molarity.getText() + "g in 1 litre = 1 molar solution\n\n";
				str_text+=txt_weight.getText() + "g in 1 millilitre = " + String.format("%1$, .4f",millisolution) + " molar solution (=" + String.format("%2.4f",(millisolution*1000)) + " millimolar solution)\n\n";
				double conc = 1 / molarity * weight * qty;
				str_text+="If you take 20 micro-litre of the stock solution and put it in 1 millilitre saline the concentration of this will be " + String.format("%10.4e", conc) + " molar\n\n";
				str_text+="* To get 1 x 10^(-2) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 100))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-3) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 1000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-4) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 10000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-5) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 100000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-6) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 1000000))) + " millilitre of saline\n";
				str_text+="\n\n";
				str_text+="* To get 1 x 10^(-2) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 100)-(qty/100))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-3) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 1000)-(qty/1000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-4) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 10000)-(qty/1000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-5) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 100000)-(qty/1000))) + " millilitre of saline\n";
				str_text+="* To get 1 x 10^(-6) molar concentration dye solution we need to dissolve " + txt_qty.getText() + " micro-litre stock solution in " + String.format("%2.4f",((conc * 1000000)-(qty/1000))) + " millilitre of saline\n";

				txa_results.setText(str_text);
			}
		});
		add(lbl_molarity);
		add(txt_molarity);
		add(lbl_weight);
		add(txt_weight);
		add(lbl_qty);
		add(txt_qty);
		add(btn_calculate);
		add(txa_results);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println(arg0);
		
	}
}
